import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
    appId: "ch.romix.korbball.meisterschaft",
    appName: "Korbball IVK",
    webDir: "build",
    bundledWebRuntime: false,
    server: { androidScheme: "http" },
};

export default config;
